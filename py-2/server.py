#!/usr/bin/env python

from flask import Flask

app = Flask(__name__)


@app.route('/')
def todo():

    return "Hello from the app-2:9001!"


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=9001, debug=True)